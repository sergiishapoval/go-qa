package typesystem

import "testing"

type Meter struct {
	value int64
}

type CentiMeter struct {
	value int64
}

func TestConversionSimilarStructsFieldsEqual(t *testing.T) {

	cm := CentiMeter{
		value: 100,
	}
	var m Meter
	//m = cm
	m = Meter(cm)

	t.Log("m.value", m.value)
	cm = CentiMeter(m)
	t.Log("cm.value", cm.value)
}

type CentiMeter32 struct {
	value int32
}

func TestConversionSimilarStructsFiledsDiff(t *testing.T) {
	cm := CentiMeter32{
		value: 100,
	}
	//var m Meter
	//m = Meter(cm)
	//
	//t.Log("m.value", m.value)
	//cm = CentiMeter(m)
	t.Log("cm.value", cm.value)
}

type MeterInt int64

type CentiMeterInt int32

func TestConversionSimilarAssignable(t *testing.T) {
	var cmInt CentiMeterInt = 100
	var mInt MeterInt
	mInt = MeterInt(cmInt)

	t.Log("mInt", mInt)
	cmInt = CentiMeterInt(mInt)
	t.Log("cmInt.value", cmInt)
}

type MyMap map[int]int

func TestAssigningUnNamedType(t *testing.T) {
	var m map[int]int = make(map[int]int)
	var mMap MyMap
	mMap = m
	t.Log(mMap)
}

type aInt int

func TestAssigningNamedType(t *testing.T) {

	i := 10
	var ai aInt = 200
	//i = ai
	i = int(ai)
	t.Log(i)
}
